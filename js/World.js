function orDefault(value, fallback) {
    return value === undefined ? fallback : value
}

class World {
    constructor(ctx, config = {}, updateDataHandler = () =>  {}) {
        // get bounds out of the drawing context
        this.bounds = {
            x: ctx.canvas.width,
            y: ctx.canvas.height
        }

        this.updateDataHandler = updateDataHandler

        this.colors = {
            red: "#FF0000",
            green: "#00FF00",
            blue: "#0000FF",
        }

        // create an array for all the species
        this.allSpecies = []

        for (const specie of orDefault(config.species)) {
            const specieCreator = () => Entity.spawn(this.bounds, { speed: 1, speedRandom: 0 })
            const species = new Species(
                orDefault(specie.name, "some species"),
                specieCreator,
                orDefault(specie.color, "#FF0000"),
                orDefault(specie.initAmount, 10),
                orDefault(config.speciesConfig, {}),
            )
            this.allSpecies.push(species)
        }

        console.log("made species")

        // add food
        this.foods = new Species(
            "food",
            () => Food.spawn(this.bounds),
            orDefault(config.foodColor, "#00FF00"),
            orDefault(config.foodInitAmount, 100)
        )
        this.foodPerStep = orDefault(config.foodPerStep, 1)
    }

    // state changer

    update() {
        const updatedData = {}

        // update all species
        this.allSpecies.forEach(species => {
            species.update(this)

            updatedData[species.name] = species.getData()
        })

        // pass data to dataHandler
        this.updateDataHandler(updatedData)

        // add food
        this.addFood()
    }

    addFood() {
        for (let i = 0; i < this.foodPerStep; i++) {
            if ((this.foodPerStep - i) > Math.random()) {
                this.foods.add()
            }
        }
    }

    // helper

    isWithinBounds(toCheck) {
        return toCheck.x > 0 && toCheck.x < this.bounds.x && toCheck.y > 0 && toCheck.y < this.bounds.y
    }

    getClosestFood(entity) {
        return this.foods.getClosest(entity)
    }
}
