class Coordinate {
    constructor(x, y) {
        this.x = x
        this.y = y
    }

    static zero() {
        return new Coordinate(0, 0)
    }

    static add(c1, c2) {
        return new Coordinate(c1.x + c2.x, c1.y + c2.y)
    }

    static sub(c1, c2) {
        return new Coordinate(c1.x - c2.x, c1.y - c2.y)
    }

    static random(start, end = null) {
        if (end === null)
            return new Coordinate(Math.random() * start.x, Math.random() * start.y)

        const size = Coordinate.sub(end, start)
        return new Coordinate(start.x + Math.random() * size.x, start.y + Math.random() * size.y)
    }

    static randomFromZero(dist) {
        const lowerBound = new Coordinate(-dist, -dist)
        const upperBound = new Coordinate(dist, dist)

        return Coordinate.random(lowerBound, upperBound)
    }

    static dist(c1, c2) {
        return Math.sqrt(Math.pow(c2.x - c1.x, 2) + Math.pow(c2.y - c1.y, 2))
    }

    static normalize(c, normalLength = 1) {
        const currLength = Coordinate.dist(c, Coordinate.zero())
        return new Coordinate(c.x / currLength * normalLength, c.y / currLength * normalLength)
    }
}
