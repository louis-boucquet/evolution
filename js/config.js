const config = {
    foodInitAmount: 8000,
    foodPerStep: 0.1,
    foodColor: "#00FF00",
    species: [
        {
            name: "some species",
            color: "#FF0000",
            initAmount: 10,
            speciesConfig: {
                deathRate: 0.3,
                birthRate: 0.3,
                costOfBirth: 0.5
            },
            entityConfig: {
                speed: 4,
                speedRandom: 10
            }
        }
    ]
}
