class Species {
    constructor(name, creator, color, initialAmount = 0, config = {}) {
        console.log("new species", { amount: initialAmount });

        this.name = name

        // create a set to contain all the entities
        this.species = new Set()

        // set entity creator
        this.creator = creator

        // fill the new set
        for (let _ = 0; _ < initialAmount; _++)
            this.add()

        this.color = color

        // some config
        this.config = {
            deathRate: 0.3,
            birthRate: 0.3,
            costOfBirth: 0.5
        }

        // override configs
        Object.assign(this.config, config)
    }

    add(entity = null) {
        if (entity === null)
            this.species.add(this.creator())
        else this.species.add(entity)
    }

    delete(entity) {
        this.species.delete(entity)
    }

    getData() {
        const avgFood = Array
            .from(this.species)
            .map(entity => entity.config.food)
            .reduce((a, b) => a + b, 0) / this.species.size

        return {
            populationSize: this.species.size,
            averageFood: avgFood,
            color: this.color
        }
    }

    render(ctx) {
        this.species.forEach(entity => entity.render(ctx))
    }

    update(world) {
        // create a new set of newborns
        this.newBorns = new Set()
        // update existing entities
        this.species.forEach(entity => entity.update(world, this))
        // add newborns
        this.newBorns.forEach(entity => this.add(entity))
    }

    getClosest(entity) {
        if (this.species.size) {
            return Array.from(this.species).reduce((f1, f2) => {
                const d1 = Coordinate.dist(entity.location, f1.location)
                const d2 = Coordinate.dist(entity.location, f2.location)

                return d1 < d2 ? f1 : f2
            })
        }
    }

    killer(entity) {
        const food = entity.config.food
        const rate = this.config.deathRate

        const percentage = 1 - food / rate

        if (Math.random() < percentage) {
            // death
            this.delete(entity)
        }
    }

    birther(entity) {
        const food = 1 - entity.config.food
        const rate = this.config.birthRate

        const percentage = 1 - food / rate

        if (Math.random() < percentage) {
            // birth
            this.newBorns.add(Entity.clone(entity))
            entity.addFood(-this.config.costOfBirth)
        }
    }
}
