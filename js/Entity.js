class Entity extends Circle {
    constructor(location, config = {}) {
        // console.log("new Entity with config", config)

        super(location)
        this.last = location

        // set config defaults
        this.config = {
            speed: 1,
            speedRandom: 2,

            food: 0.6,
            foodValue: 0.1,
            foodEatDistance: 10,
            foodBurnRate: 0.001,
        }

        // override configs
        Object.assign(this.config, config)
    }

    update(world, species) {
        // remember last location
        this.last = this.location

        const closestFood = world.getClosestFood(this)

        // if there is no closestFood, don't move
        const targetLocation = closestFood === undefined ? this.location : closestFood.location

        // calculate speed
        let speedDirection = Coordinate.sub(targetLocation, this.location)
        speedDirection = Coordinate.normalize(speedDirection, this.config.speed)
        
        speedDirection = Coordinate.add(speedDirection, Coordinate.randomFromZero(this.config.speedRandom))
        // apply speed by moving
        this.location = Coordinate.add(this.location, speedDirection)

        if (!world.isWithinBounds(this.location)) {
            this.location = Coordinate.random(world.bounds)
        }

        this.addFood(-Coordinate.dist(this.location, this.last) * this.config.foodBurnRate)

        // if (!species.killer(this)) {
        //     species.birther(this)
        //     this.eat(closestFood, world)
        // }

        species.killer(this)
        species.birther(this)

        if (closestFood) {
            this.eat(closestFood, world)
        }
    }

    eat(closestFood, world) {
        if (Coordinate.dist(closestFood.location, this.location) <= this.config.foodEatDistance) {
            world.foods.delete(closestFood)
            this.addFood(this.config.foodValue)
        }
    }

    addFood(amount) {
        this.config.food += amount
        this.config.food = Math.max(0, this.config.food)
        this.config.food = Math.min(1, this.config.food)
        // console.log({amount, food: this.config.food})
    }

    static spawn(bounds, config = {}) {
        return new Entity(Coordinate.random(bounds), config)
    }

    static clone(entity) {
        return new Entity(entity.location, entity.config)
    }
}
