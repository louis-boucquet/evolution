class Circle {
    constructor(location, speed = { x: 0, y: 0 }, acceleration = { x: 0, y: 0 }, size = 10) {
        this.location = location
        this.size = size
    }

    render(ctx) {
        ctx.beginPath()
        ctx.arc(this.location.x, this.location.y, this.size, 0, 2 * Math.PI)

        ctx.fill()
    }

    static spawn(border) {
        return new Circle()
    }
}
