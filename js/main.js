"use strict";

document.addEventListener("DOMContentLoaded", init)

function init() {
    // getting canvas 
    const canvas = document.getElementById("universe")
    const ctx = canvas.getContext("2d")

    // making a new plot
    Plotly.newPlot('population', [{ y: [] }])
    Plotly.newPlot('food', [{ y: [] }])

    // creating new world (duuh)
    const world = new World(ctx, config, updateDataHandler)

    let step = 0

    // standard animantion function
    function update() {

        // clear previous drawings
        ctx.clearRect(0, 0, 800, 800)

        // ENTITIES

        // draw all the species with theit specific color
        world.allSpecies.forEach(species => {
            ctx.fillStyle = species.color
            species.render(ctx)
        })

        // FOOD

        // draw each food in green
        ctx.fillStyle = world.foods.color
        world.foods.render(ctx)

        // update the world
        world.update()

        // if (step < 1)
        window.requestAnimationFrame(update)

        step++
    }

    window.requestAnimationFrame(update)
}
