const data = {}

for (const { name } of config.species) {
    data[name] = {
        population: [],
        food: []
    }
}

const populationLayout = {
    title: 'Population in function of time',
    titlefont: {
        size: 22,
    },
    xaxis: {
        title: 'Time step',
        showgrid: false,
        zeroline: false
    },
    yaxis: {
        title: 'Population',
        showline: false
    }
}

const foodLayout = {
    title: 'Average food in function of time',
    titlefont: {
        size: 22,
    },
    xaxis: {
        title: 'Time step',
        showgrid: false,
        zeroline: false
    },
    yaxis: {
        title: 'Food',
        range: [0, 1],
        showline: false
    }
}

function updatePopulationPlot(data) {
    Plotly.react('population', data, populationLayout)
}

function updateFoodPlot(data) {
    Plotly.react('food', data, foodLayout)
}

function updateDataHandler(updatedData) {
    const populationData = []
    const foodData = []

    for (const species in updatedData) {
        const speciesData = updatedData[species]

        const population = data[species].population.slice(0)
        population.push(speciesData.populationSize)
        data[species].population = population

        populationData.push({
            y: population,
            name: species,
            line: {
                color: speciesData.color
            }
        })

        const food = data[species].food.slice(0)
        food.push(speciesData.averageFood)
        data[species].food = food

        foodData.push({
            y: food,
            name: species,
            line: {
                color: speciesData.color
            }
        })

    }

    updatePopulationPlot(populationData)
    updateFoodPlot(foodData)
}
