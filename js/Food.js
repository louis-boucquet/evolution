class Food extends Circle {
    static spawn(bounds) {
        return new Food(Coordinate.random(bounds), { x: 0, y: 0 }, { x: 0, y: 0 }, 5)
    }
}
