# Balance of Species

Welcome, I've made world simulator.  
In this simulator you can put artificialy created species.

The idea is to change some parameters in the config file en see what effect it has on the population of the species.

## Startup

To get started, just double click (or otherwise open) `./index.html` in your browser.

## the config file

The config file can be found in `./js/config.js`.

**Note:** that you don't have to fill in everything, all fields have default fallbacks!

I have put ranges with every numerical parameter, `[a - b]` means between `a` and `b`, if either one of those numbers isn't filled in, that means the bound is infinity

These are all configurable parameters and their defaults

```js
const config = {
    // amount of food on time step one [0 - ]
    foodInitAmount: 100,
    // amount of food added per time step [0 - ]
    foodPerStep: 1,
    // the color of the food (in this case green)
    foodColor: "#00FF00",
    // present species (default is empty)
    species: [
        {
            // name of a species
            name: "some species",
            // color of a species
            color: "#FF0000",
            // amount of this species on time step one [0 - ]
            initAmount: 10,
            // configuration of the species as a whole
            speciesConfig: {
                // how much death occurs [0 - 1]
                deathRate: 0.3,
                // how birth death occurs [0 - 1]
                birthRate: 0.3,
                // how much food an entity lose with birth [0 - 1]
                costOfBirth: 0.5
            },
            // configuration of individual entities (these can change for children)
            entityConfig: {
                // how fast an entity moves [ - ]
                speed: 1,
                // randomness of moving [ - ]
                speedRandom: 2,
                // initial food [0 - 1]
                food: 0.6,
                // the amount food you get by eating one food [0 - ]
                foodValue: 0.1,
                // the distance from which you can eat your food [1 - ]
                foodEatDistance: 10,
                // how much food it costs to move (distanceMoved * foodBurnRate) [0 - ]
                foodBurnRate: 0.001,
            }
        }, {
            name: "blue",
            color: "#0000FF",
            initAmount: 10
        }
    ]
}
```

## how does it work

just like that
